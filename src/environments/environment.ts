// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB5S7RT9sDvbQhPn8JSjwkm6ci4GmfwIkI",
    authDomain: "foss-50bed.firebaseapp.com",
    projectId: "foss-50bed",
    storageBucket: "foss-50bed.appspot.com",
    messagingSenderId: "45679572261",
    appId: "1:45679572261:web:a2b15aac7a4f6589a656cc",
    measurementId: "G-BYLSNRVGWK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
