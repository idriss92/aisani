import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// firebase
import {provideFirebaseApp, getApp, initializeApp} from '@angular/fire/app';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeroComponent } from './components/hero/hero.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { StoreModule } from '@ngrx/store';
import { contactusReducer } from './store/contactus.reducer';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { ContactUsEffects } from './store/contactus.effects';

// const config = {
//   apiKey: process.env['FIREBASE_APIKEY'],
//   authDomain: process.env['FIREBASE_AUTHDOMAIN'],
//   projectId: process.env['FIREBASE_PROJECTID'],
//   storageBucket: process.env['FIREBASE_STORAGEBUCKET'],
//   messagingSenderId: process.env['FIREBASE_MESSAGESENDERID'],
//   appId: process.env['FIREBASE_APPID'],
//   measurementId: process.env['FIREBASE_MEASUREMENTID']
// }

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FooterComponent,
    HeroComponent,
    TestimonialsComponent,
    ContactusComponent
  ],
  imports: [
    // provideFirebaseApp(() => initializeApp(config)),
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({contactus: contactusReducer}),
    EffectsModule.forRoot([ContactUsEffects]),
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent,
    NavigationComponent,
    FooterComponent,
    HeroComponent,
    ContactusComponent
]
})
export class AppModule { }
