import { Injectable } from "@angular/core";
import { Actions } from "@ngrx/effects";
import { createEffect, ofType } from "@ngrx/effects";
import { map, switchMap, catchError } from "rxjs";
import { reset, submit, submitError, submitSuccess } from "./contactus.action";
import { ContactAPIService } from "../services/contact.service";
import { IAppState } from "../models/contactus/IAppState";
import { Store } from "@ngrx/store";


@Injectable()
export class ContactUsEffects {
    constructor(
        private actions$: Actions,
        private api: ContactAPIService,
        private contactStore: Store<IAppState>,
        private store: Store
    ) { }

    submitContact$ = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(submit),
                switchMap((action) => {
                    // this.store.dispatch()
                    return this.api.submit(action.payload).pipe(
                        map((data) => {
                            console.log("success")
                            return submitSuccess();
                        }),
                        catchError(async (error) => {
                            console.error(error);
                            return submitError()
                        })
                    )
                })
            )
        }
    )
}