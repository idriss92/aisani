import {createAction, props} from '@ngrx/store';
import { IContactState } from '../models/contactus/IContactState';

export const reset = createAction('[Contact Us Component] Reset');

export const updateForm = createAction('[Contact Us Component] Update Form',
    props<{payload:{name: string, value: string} }>()
)

export const submit = createAction('[Contact Us Component] Submit',
    props<{payload: IContactState}>()
);

export const submitSuccess = createAction('[Contact Us Component] submit success');

export const submitError = createAction('[Contact Us Component] submit error');