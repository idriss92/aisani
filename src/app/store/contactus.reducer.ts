import { createReducer, on } from '@ngrx/store';
import { reset, updateForm, submit } from "./contactus.action";
import { IContactState } from '../models/contactus/IContactState';
import { IAppState } from "../models/contactus/IAppState";

export const contactState: IContactState = {
    name: '',
    phoneNumber: 0,
    mailAdress: '',
    expectations: ''
}

export const initialState: IAppState = {
    form: contactState,
    isSend: false
}

export const contactusReducer = createReducer(
    initialState,
    on(reset, (state) => initialState),
    on(updateForm, (state: IAppState, { payload }) => ({
        ...state,
        form: {
            ...state.form,
            [payload.name]: payload.value
        }
    })),
    on(submit, (state) => ({
        ...state,
        isSend: true,
        form: contactState
    }))
)