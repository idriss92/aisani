import {createSelector, createFeatureSelector} from '@ngrx/store';
import { IContactState } from '../models/contactus/IContactState';
import { IAppState } from "../models/contactus/IAppState";

export const appState = createFeatureSelector<IAppState>("contactus");

export const formContact = createSelector(appState, (state) => state.form);

export const selectContactUsState = (state: IAppState) => state;

export const selectName = createSelector(formContact, (state) => state.name);
export const selectMail = createSelector(formContact, state => state.mailAdress);
export const selectExpectation = createSelector(formContact, state => state.expectations);
export const selectPhoneNumber = createSelector(formContact, state => state.phoneNumber);

export const contactFormIsSend = (state: IAppState) => state.isSend;
