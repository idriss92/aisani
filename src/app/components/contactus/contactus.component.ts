import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { IContactState } from '../../models/contactus/IContactState';
import { IAppState } from "../../models/contactus/IAppState";
// import { ContactUsStore } from '../../store/contactus.store';
import { Store, select } from '@ngrx/store';
import { reset, submit, updateForm } from '../../store/contactus.action';
import { selectExpectation, selectMail, selectName, selectPhoneNumber, formContact } from '../../store/contact.select';
import { combineLatest, Subscription } from 'rxjs'
import { filter } from 'rxjs/operators'

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactusComponent implements OnInit, OnDestroy {

  protected subscriptions: Subscription[] = [];
  constructor(private contactStore: Store<IAppState>,
    private store: Store) { }

  contactForm$ = this.contactStore.pipe(select(formContact));
  name$ = this.contactStore.pipe(select(selectName));
  phoneNumber$ = this.contactStore.pipe(select(selectPhoneNumber));
  mailAdress$ = this.contactStore.pipe(select(selectMail));
  expectations$ = this.contactStore.pipe(select(selectExpectation));
  contactForm!: IContactState;
  name!: string;
  phoneNumber!: number;
  mailAdress!: string;
  expectations!: string;

  ngOnDestroy(): void {
    for (let s of this.subscriptions)
      s.unsubscribe();
  }
  ngOnInit(): void {
    this.reset();

    this.subscriptions.push(

      this.contactForm$.subscribe((form) => {
        this.contactForm = form;
      }),

      combineLatest([this.name$, this.phoneNumber$, this.mailAdress$, this.expectations$])
        .pipe(filter(x => !!x && x[0] != null && x[2] != null && x[3] != null))
        .subscribe(([name, phoneNumber, mailAdress, expectations]) => {
          this.name = name;
          this.phoneNumber = phoneNumber;
          this.mailAdress = mailAdress;
          this.expectations = expectations
        })
    )
  }

  reset() {
    this.store.dispatch(reset())
  }

  updateForm($event: any) {
    console.log($event)
    this.store.dispatch(updateForm({ payload: { name: $event.target.name, value: $event.target.value } }))
  }

  submit() {
    this.store.dispatch(submit({ payload: this.contactForm }))
  }
}
