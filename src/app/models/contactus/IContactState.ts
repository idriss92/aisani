export interface IContactState {
    name: string;
    phoneNumber: number;
    mailAdress: string;
    expectations: string;
}